package com.academiasmoviles.lab3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Switch
import kotlinx.android.synthetic.main.activity_destino.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        val bundle : Bundle? = intent.extras

        bundle?.let { bundle ->

            val nombre =  bundle.getString("key_nombre","No especificado")
            val edad    = bundle.getString("key_edad","0")
            val mascota  = bundle.getString("key_mascota","")
            val chk1 = bundle.getString("key_chk1","0")
            val chk2 = bundle.getString("key_chk2","0")
            val chk3 = bundle.getString("key_chk3","0")
            val chk4 = bundle.getString("key_chk4","0")
            val chk5 = bundle.getString("key_chk5","0")

            tvNombre.text = "Nombre: $nombre"
            tvEdad.text = "Edad: $edad"


            when (mascota)
            {
                in "P" -> imgResultado.setImageResource(R.drawable.dog)
                in "G" -> imgResultado.setImageResource(R.drawable.gato)
                in "C" -> imgResultado.setImageResource(R.drawable.conejo)

            }

            /*
            if (mascota == "P") imgResultado.setImageResource(R.drawable.dog)
            if (mascota == "G") imgResultado.setImageResource(R.drawable.gato)
            if (mascota == "C") imgResultado.setImageResource(R.drawable.conejo)
             */

            if (chk1 == "1") chkUno.isChecked = true
            if (chk2 == "1") chkDos.isChecked = true
            if (chk3 == "1") chkTres.isChecked = true
            if (chk4 == "1") chkCuatro.isChecked = true
            if (chk5 == "1") chkCinco.isChecked = true


        }



    }
}