package com.academiasmoviles.lab3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {

            var nombre = edtNombres.text.toString()
            var edad = edtEdad.text.toString()

            if (nombre.isEmpty()) {
                Toast.makeText(this,"Debe ingresar un nombre",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (edad.isEmpty()) {
                Toast.makeText(this,"Debe ingresar la edad",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if(!rbPerro.isChecked && !rbGato.isChecked && !rbConejo.isChecked){
                Toast.makeText(this,"Debe seleccionar alguna mascota",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if(!chkUno.isChecked && !chkDos.isChecked && !chkTres.isChecked && !chkCuatro.isChecked && !chkCinco.isChecked) {
                Toast.makeText(this,"Debe de seleccionar alguno de los valores",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var mascota = if (rbPerro.isChecked) "P" else if (rbGato.isChecked) "G" else "C"
            var chk1 = if(chkUno.isChecked)    "1" else "0"
            var chk2 = if(chkDos.isChecked)    "1" else "0"
            var chk3 = if(chkTres.isChecked)   "1" else "0"
            var chk4 = if(chkCuatro.isChecked) "1" else "0"
            var chk5 = if(chkCinco.isChecked)  "1" else "0"


            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre",nombre)
                putString("key_edad",edad)
                putString("key_mascota",mascota)
                putString("key_chk1",chk1)
                putString("key_chk2",chk2)
                putString("key_chk3",chk3)
                putString("key_chk4",chk4)
                putString("key_chk5",chk5)
            }

            val intent = Intent(this,DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)








        }




    }
}